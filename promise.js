const { resolve } = require("path");

const funct1 = async()=> {
    return new Promise((resolve,reject) => {
        setTimeout(() => {
            if(25 != 5) // try it with changing the condition
            {
                {resolve('It is true'); }
            }
            else
            {
                {reject('It is false'); }
            }
        },2000);
        // setTimeout(function() {resolve('It is true')},2000);
    })
}


setInterval(myTimer, 2000);// setInterval executes the mytimer function every 2 seconds(2000 milli seconds)

function myTimer() {
  const date = new Date();
  const a = date.toLocaleTimeString();
  console.log(a);
}

// since func1 is returning a promise, if the promise if fulfilled i.e; resolve, func1().then() catches the resolve statement. 
// Else, .catch() catches the else case i.e; reject statement.
funct1().then((ret) => {
    console.log(`success: ${ret}`);
}).catch((error) => {
    console.error(`err:${error}`);
})