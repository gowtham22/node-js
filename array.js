const fruits = ['banana', 'apple'];

// forEach acts like a for loop, selects first index and triggers the callback function and once the callback function is executed, 
//the index value is updated and the element in the next index is sent as an arg to the callback function. Callback function will be called fruits.length times 
fruits.forEach(updateElement);
// join helps you with joining all the elements of the array
console.log("all together",fruits.join());



function updateElement(value1,value2){
    console.log(fruits[value2]);
    fruits[value2] = value1 +"\t"+ value2;
    console.log(fruits[value2]);
}



const addElement = async() => {
    return new Promise((resolve,reject) =>{
        resolve(fruits.push("mango\t2"));// pushing new element to the array
        // resolve(fruits.unshift("mango\t2")); //add new element at the 0 index of the array and rest of the elements take the higher index.
        // resolve(fruits.pop()); // removes the element from last index.
    }) 
    ;
}

addElement().then((value) => {
    console.log(fruits[fruits.length - 1]);
    console.log(fruits.join());
});






