const express = require('express');
const router = express.Router();

const user = require('../controller/service')
const card = require('../controller/GradeSheetService')

router.get('/', user.index)
router.post('/show', user.show)
router.post('/insertData', user.insertData)
router.post('/insertMultipleData', user.insertMultipleData)
router.get('/filter', user.filter)
router.get('/LookUp', user.lookUp)
router.post('/updateUser', user.updateUser)
router.post('/remove', user.remove)


router.get('/card',card.index)
router.post('/show_card', card.show)
router.post('/insertData_card', card.insertData)
router.post('/update_card', card.updateCard)
router.post('/remove_card', card.remove)
router.post('/insert',card._insert)
module.exports = router;
