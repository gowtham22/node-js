
// // returns a global object.
// // console.log(global);


// const os = require('os');
// // returns os type
// console.log(os.type());
// // returns os version
// console.log(os.version());
// // returns home dir
// console.log(os.homedir());
// // returns home dir
// console.log(__dirname);
// // returns filename
// console.log(__filename);
// // add imported from math.js
// const {add} = require("./math");
// console.log("2 + 3 =",add(2,3));


//start a server 
// const http = require('http');
const express =require('express');
const bodyParser = require('body-parser');
const connectDb = require("./mongo.js");
const router = require("./routes/routes.js")
const port = 3000;
const app = express()
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:false}));

// const server = http.createServer(async (req, res) => {   
//     await connectDb;
//     res.statusCode = 200; 
//     res.setHeader('Content-Type', 'text/plain');
//     res.end('Welcome to Gowtham City');
// });
app.use(router)



app.listen(port, () => {
    console.log(`Server running at ${port}`);
});