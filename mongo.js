const mongoose = require('mongoose')


const connectDb = async(req,res,next)=>{
    await mongoose.connect("mongodb://localhost:27017/test",{
    useNewUrlParser: true,
    useUnifiedTopology: true,
  }, (err) => {
    if(err)
    {
        console.log(err);
    }
    else
    {
        console.log("mongo db connected");
    }
})
}

module.exports = connectDb();
