const { Timestamp } = require('mongodb');
const mongoose = require('mongoose')

const gradeSheet = new mongoose.Schema({
    name: {type: String},
    id: {type: Number},
    hindi: {type: String},
    english: {type: String},
    Math: {type: String},
    grade: {type: String}
},{timestamps: true})

module.exports =  mongoose.model("gradesheet",gradeSheet)