const mongoose = require('mongoose');


const userData =  new mongoose.Schema({
    name: {type : String},
    id: {type: Number, required:true, unique: true},
    phoneNum: {type: Number},
    gender: {type: String},
    age: {type: Number}
},{timestamps: true})

module.exports = mongoose.model("userData",userData);