const fs = require("fs");
const path = require("path");
fs.readFile(path.join(__dirname,'files','read.txt'),'utf8',(err,data)=>{
    if(err) throw err;
    console.log(data.toString());
})

console.log("hello");


fs.writeFile(path.join(__dirname,'files','write.txt'),'WELCOME TO NODE JS',(err)=>{
    if(err) throw err;
    console.log('Write done');

    fs.appendFile(path.join(__dirname,'files','write.txt'),'\nNICE TO MEET YOU ',(err)=>{
        if(err) throw err;
        console.log('append done');
    })
})

fs.appendFile(path.join(__dirname,'files','test.txt'),'WELCOME ',(err)=>{
    if(err) throw err;
    console.log('append done');
})


process.on('uncaughtException', err =>{
    console.error(`wert: ${err}`);
    process.exit(1);
})