const mongoose = require("mongoose");
const model = require("../model/gradesheet");

exports.index = (req,res,next)=>{

    
    model.find().then((response) => {
        res.json({
            response
        })
    }).catch((err)=>{
        res.json({
            message: 'no data found'
        })
    })
}


exports.show = (req,res,next) => {
        let cardID = req.body.cardID
        model.find({id:cardID}).then((response) => {
            res.json({
                response
            })
        }).catch((err) => {
            res.json({
                message: 'no data found'
            })
            })
}

exports.insertData = (req,res,next) => {
    let newCard = new model({
        name: req.body.name,
        id: req.body.id,
        hindi: req.body.hindi,
        english: req.body.english,
        Math: req.body.Math,
        grade: req.body.grade
    })    
    model.create(newCard).then((response) => {
           res.json({
               response,
               message: "card added successfully"
           }) 
        }).catch((err) =>{
            res.json({
                message: 'card not added successfully'
            })
        })
}

exports.updateCard = (req,res,next) => {
        let cardID = req.body.cardID

        let a = ({
            name: req.body.name,
            id: req.body.id,
            hindi: req.body.hindi,
            english: req.body.english,
            Math: req.body.english,
            grade: req.body.grade
        })

        model.findOneAndUpdate({id:cardID}, {$set: a}).then((response) => {
            res.json({
                response,
                message: "card updated successfully"
            }) 
        }).catch((err) => {
            res.json({
                message: 'card not updated successfully'
            })
        })

}

exports.remove = (req,res,next) => {
    let cardID = req.body.cardID
    model.findOneAndDelete({id:cardID}).then(() => {
        res.json({
            message: "card removed successfully"
        }) 
    }).catch(() => {
        res.json({
            message: 'card not updated successfully'
        })
    })
}

exports._insert = (req,res,next) => {
    const a = {"creditHindi":"A"}
    model.collection.insertOne({a}).then(() => {
        res.json({
            message: "inserted successfully"
        }) 
    }).catch(() => {
        res.json({
            message: "Not inserted "
        }) 
    })
}
