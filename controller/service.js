const mongoose = require("mongoose");
const model = require("../model/schema");
const gradeModel = require("../model/gradesheet");
const { response } = require("express");
const { id } = require("date-fns/locale");
const gradesheet = require("../model/gradesheet");
const bcrypt = require("bcrypt");

exports.index = (req,res,next)=>{
    userData.find().then((response) => {
        res.json({
            response
        })
    }).catch((err)=>{
        res,json({
            message: 'no data found'
        })
    })
}


exports.show = (req,res,next) => {
        let userID = req.body.userID
        userData.find({id:userID}).then((response) => {
            res.json({
                response
            })
        }).catch((err) => {
            res.json({
                message: 'no data found'
            })
            })
}

exports.insertData = async(req,res,next) => {
    console.log("==================>",await req.body.name);

    // try{
    //     const salt =await  bcrypt.genSalt(10);
    //     const hashedPassword =await bcrypt.hash(req.body.password,salt);
    //     console.log("salt",salt);
    //     console.log("hashedPassword",hashedPassword);
    let newUser = {
        name: req.body.name,
        id: req.body.id,
        phoneNum: req.body.phoneNum,
        gender:req.body.gender,
        age:req.body.age
    }

     
    model.collection.insertOne(newUser).then((response) => {
           res.json({
               message: "user added successfully"
           }) 
        }).catch((err) =>{
            console.log(err),
            res.json({
                message: 'user not added successfully'
            })
        })
    // }
    // catch(error){
    //     next(error)
    // }  
}

exports.insertMultipleData = (req,res,next) => {
    var i
    let array = []
    for(i = 0;i<req.body.length;i++){
    console.log("==================>",req.body[i].name);
        let newUser = new model({
                name: req.body[i].name,
                id: req.body[i].id,
                phoneNum: req.body[i].phoneNum,
                gender:req.body[i].gender,
                age:req.body[i].age,
                userName: req.body[i].userName,
                password: req.body[i].password
            })
        array.push(newUser)   
    }  
    userData.collection.insertMany(array).then((response) => {
           res.json({
               response,
               message: "user added successfully"
           }) 
        }).catch((err) =>{
            console.log(err),
            res.json({
                message: 'user not added successfully'
            })
        })
}


exports.filter = (req,res,next) => {
    userData.aggregate([
        {
            $match : {age : 28}
        }
    ]).then((resp) => {
        console.log(resp)
            res.json({
                resp
            })
    }).catch((err) => {
        console.log(err)
        res.json({
            message: "No data found"
        })
    })
}

exports.lookUp = (req,res,next) => {
    userData.aggregate([
        {
            $lookup : {
                from: "gradesheet",
                localField: "id",
                foreignField: "id",
                as: "anything"
            }
        },
        {
            $group :{
                _id: "$age",
                totalMale:{
                    $sum: {
                        $cond: [{$eq:[ "$gender", "Male" ]},1,0]
                    }
                }
            }
        }
    ]).then((resp) => {
        console.log(resp)
            res.json({
                resp
            })
    }).catch((err) => {
        console.log(err)
        res.json({
            message: "No data found"
        })
    })
}

exports.updateUser = (req,res,next) => {
        let userID = req.body.userID

        let a = ({
            name: req.body.name,
            id: req.body.id,
            phoneNum: req.body.phoneNum,
            gender:req.body.gender,
            age:req.body.age,
            userName: req.body.userName,
            password: req.body.password
        })

        userData.findOneAndUpdate({id:userID}, {$set: a}).then((response) => {
            res.json({
                response,
                message: "user updated successfully"
            }) 
        }).catch((err) => {
            res.json({
                message: 'user not updated successfully'
            })
        })

}

exports.remove = (req,res,next) => {
    let userID = req.body.userID
    userData.findOneAndDelete({id:userID}).then(() => {
        res.json({
            message: "user removed successfully"
        }) 
    }).catch(() => {
        res.json({
            message: 'user not updated successfully'
        })
    })
}


