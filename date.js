const {format} = require('date-fns');
const {v4: uuid} = require('uuid');
const fs = require('fs');
const fsPromises = require('fs').promises;
const path = require('path');

const logEvents = async (message) =>{
    const d = `${format( new Date() ,'yyyyMMdd\tHH:mm:ss')}`;
    const f = `${d}\t${uuid()}\t${message}`;
    console.log(f);
    try{
        await fsPromises.appendFile(path.join(__dirname,"logEvents","eventsLog.txt"),f);
    }
    catch (err){
        console.log(err);
    }
}
// logEvents("gowtham")


console.log(format( new Date() ,'yyyyMMdd\tHH:mm:ss'));
console.log(uuid());
console.log("gowtham city");

module.exports = logEvents;